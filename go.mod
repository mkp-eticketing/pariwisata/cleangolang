module sample

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/go-redis/redis/v8 v8.8.0
	github.com/gomodule/redigo v1.8.4
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/lib/pq v1.10.0
	github.com/nitishm/go-rejson/v4 v4.0.0
	github.com/tkanos/gonfig v0.0.0-20210106201359-53e13348de2f
	github.com/valyala/fasttemplate v1.2.1 // indirect
	go.mongodb.org/mongo-driver v1.5.0
	golang.org/x/crypto v0.0.0-20210317152858-513c2a44f670 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
)
