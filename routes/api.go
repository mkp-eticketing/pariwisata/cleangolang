package routes

import (
	"net/http"
	"sample/config"
	"sample/services"
	"sample/services/pedagangKiosGradingService"
	"sample/services/pedagangKiosPoinService.go"
	"sample/services/productService"
	"sample/services/warehouseService"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

// RoutesApi
func RoutesApi(e echo.Echo, usecaseSvc services.UsecaseService) {

	public := e.Group("/public")

	productSvc := productService.NewProductService(usecaseSvc)
	productGroup := public.Group("/product")
	productGroup.POST("/add", productSvc.AddProduct)
	productGroup.POST("/update", productSvc.UpdateProduct)

	productMongoSvc := productService.NewProductMongoService(usecaseSvc)
	productMongoGroup := public.Group("/product/mongo")
	productMongoGroup.POST("/add", productMongoSvc.AddProductMongo)

	warehouseSvc := warehouseService.NewWarehouseService(usecaseSvc)
	warehouseGroup := public.Group("/warehouse")
	warehouseGroup.POST("/add", warehouseSvc.AddWarehouse)

	pedagangKiosPoinSvc := pedagangKiosPoinService.NewPedagangKiosPoinService(usecaseSvc)
	pedagangKiosPoinGroup := public.Group("/pedagangkiospoin")
	pedagangKiosPoinGroup.GET("/daily", pedagangKiosPoinSvc.AddPedagangKiosPoinDaily)

	pedagangKiosGradingSvc := pedagangKiosGradingService.NewPedagangKiosGradingService(usecaseSvc)
	pedagangKiosGradingGroup := public.Group("/pedagangkiosgrading")
	pedagangKiosGradingGroup.GET("/daily", pedagangKiosGradingSvc.AddPedagangKiosGradingWeekly)

	private := e.Group("/private")
	private.Use(middleware.JWT([]byte(config.GetEnv("JWT_KEY"))))
	private.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowCredentials: true,
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))

}
